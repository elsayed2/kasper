# Kasper Website

This template is built using only HTML and CSS. It provides a clean, modern design for a web agency or similar business. The template includes various sections such as services, portfolio, about, and contact. No additional functionality is implemented beyond the visual design and layout.

## Live Demo

[Live Demo](https://kasper-elsayed2-37a36fff4764422c305d7eed71d1880030e545ec9b1af40.gitlab.io/)

## Usage

To use this template, simply open the `index.html` file in your browser. Ensure that the CSS and image files are located in the appropriate directories as specified in the HTML code.

## Template Structure

- **Header:** Contains the navigation menu.
- **Landing:** Introductory section with a welcome message.
- **Services:** Detailed list of services offered.
- **Portfolio:** Showcase of completed projects.
- **Design:** Highlights key design features.
- **About:** Information about the business.
- **Stats:** Business statistics.
- **Skills:** Display of key skills and testimonials.
- **Quote:** Featured quote section.
- **Pricing:** Pricing plans for services.
- **Subscribe:** Email subscription form.
- **Contact:** Contact information and social media links.
- **Footer:** Footer with logo, social icons, and copyright information.


